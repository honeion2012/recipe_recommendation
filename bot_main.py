# -*- coding: utf-8 -*-
from flask import Flask
from slack import WebClient
from slackeventsapi import SlackEventAdapter
from slack.web.classes.blocks import *
from utils import search, create_dict, VariableObj, read_category
from command import help_guide, print_category, print_recipe
from config import SLACK_TOKEN, SLACK_SIGNING_SECRET

vars = VariableObj()

app = Flask(__name__)
# /listening 으로 슬랙 이벤트를 받습니다.
slack_events_adaptor = SlackEventAdapter(SLACK_SIGNING_SECRET, "/listening", app)
slack_web_client = WebClient(token=SLACK_TOKEN)

# 챗봇이 멘션을 받았을 경우 # 명령어에 따라 멘션을 다르게 해야해
@slack_events_adaptor.on("app_mention")
def app_mentioned(event_data):
    channel = event_data["event"]["channel"]
    text = event_data["event"]["text"]
    print(text)
    text = text[13:] #.replace('<@UKY78NRQA> ', '')
    case = input_command(text, channel)
    print(case)
    SEARCHED, index = vars.get_searched(), vars.get_start()
    print(SEARCHED, index)
    #케이스값 주고, 1h2c3s4n5p6q
    if SEARCHED == False : #1
        if case in [0,1]:
            home_display = create_dict('data/home.json')
            print_category(home_display, SEARCHED)
            slack_web_client.chat_postMessage(
                channel = channel,
                blocks = extract_json(home_display)
            )
    else : #3,4,5
        if case == 3 : #새로 검색하는게 맞고
            # 주어진 텍스트로 검색한 결과 중 탑3를 추출합니다.
            word_message, image_message, material_message = search(text)
            vars.set_word(word_message)
            vars.set_image(image_message)
            vars.set_material(material_message)
            # result.json 블록을 갱신합니다.
            res_display = create_dict('data/result.json')
            print_category(res_display, SEARCHED)
            print_recipe(res_display, vars.get_word(), vars.get_material(), vars.get_image(), index)
            slack_web_client.chat_postMessage(
                channel=channel,
                blocks= extract_json(res_display)
            )
        elif case in [4,5]:#이미 검색한 결과를 가져다 써야하는데
            res_display = create_dict('data/result.json')
            print_category(res_display, SEARCHED)
            print_recipe(res_display, vars.get_word(), vars.get_material(), vars.get_image(), index)
            slack_web_client.chat_postMessage(
                channel=channel,
                blocks=extract_json(res_display)
            )


def input_command(command, channel):
    case = 0
    default_query = ""
    query = None
    print(command)
    if command.startswith("-"):
        if command == "-help":
            case = 1
            vars.set_searched(False)
            query = help_guide()
            respond(channel, query)
            return case
        if command == "-category":
            case = 2
            vars.set_searched(False)
            home_display = create_dict('data/home.json')
            home_display[3]['text']['text'] = read_category(True)
            slack_web_client.chat_postMessage(
                channel=channel,
                blocks=extract_json(home_display),
                text=""
            )
            return case
        if command.startswith("-s"):
            case = 3
            vars.set_searched(True)
            vars.set_start(0)
            return case
        if command == "-next":
            case = 4
            if vars.get_searched():
                vars.set_start((vars.get_start()+3)%12)
                print("next : ", vars.get_start())
                return case
            else :
                query = "추천 레시피를 보고 싶으시다면 검색을 먼저 부탁드립니다.\n"
                respond(channel, query)
                return case
        if command == "-prev":
            case = 5
            if vars.get_searched():
                vars.set_start((vars.get_start() - 3) % 12)
                print("prev : ", vars.get_start())
                return case
            else :
                query = "추천 레시피를 보고 싶으시다면 검색을 먼저 부탁드립니다.\n"
                respond(channel, query)
                return case
        if command == "-q":
            case = 6
            vars.set_searched(False)
            query = "종료합니다."
            respond(channel, query)
            return case
        if command == None:
            vars.set_searched(False)
            query = "유효하지 않은 명령어입니다. -help를 입력해 명령어를 확인해 보세요.\n"
            respond(channel, query)
    else:
        query = "유효하지 않은 명령어입니다. -help를 입력해 명령어를 확인해 보세요.\n"
        respond(channel, query)
    return case


def respond(channel, query):
    slack_web_client.chat_postMessage(
        channel=channel,
        text=query or ""
    )
# / 로 접속하면 서버가 준비되었다고 알려줍니다.
@app.route("/", methods=["GET"])
def index():
    return "<h1>Server is ready! no....</h1>"

if __name__ == '__main__':
    app.run('0.0.0.0', port=5000)